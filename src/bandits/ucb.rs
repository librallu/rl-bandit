use crate::bandit::{Bandit, Action, Reward, update_average};

/**
 * Implements an UCB arm
 */
struct UCBArm {
    /// number of trials so far
    n: u64,
    /// average reward
    q: Reward
}

impl UCBArm {
    pub fn new(n: u64, q: Reward) -> Self {
        Self { n:n, q:q }
    }
}

/**
 * Implements an Epsilon-Greedy algorithm
 */
pub struct UCB {
    c: f64,
    nb_steps: u64,
    t: Vec<UCBArm>

}

impl UCB {
    pub fn new(n: Action, c: f64) -> Self {
        let mut t = Vec::new();
        for _ in 0..n {
            t.push(UCBArm::new(0, 0.));
        }
        Self { c:c, nb_steps: 0, t:t }
    }

    fn arm_value(&self, e:&UCBArm) -> f64 {
        return e.q + self.c*((self.nb_steps as f64).ln()/(e.n as f64)).sqrt()
    }
}

impl Bandit for UCB {

    fn choose(&self) -> Action {
        let mut maxi = 0;
        let mut maxi_val = self.arm_value(&self.t[0]);
        for (i,e) in self.t.iter().enumerate() {
            let val = self.arm_value(&e);
            if maxi_val < val {
                maxi = i;
                maxi_val = val;
            }
        }
        return maxi;
    }

    fn update(&mut self, a: Action, r: Reward) {
        self.nb_steps += 1;
        self.t[a].n += 1;
        self.t[a].q = update_average(self.t[a].q, r, self.t[a].n);
    }

    fn str(&self) -> std::string::String {
        return format!("UCB c={:.2}", self.c);
    }

}